package com.android.redditcatviewer;

/**
 * Created by kscheib on 4/15/15.
 */
public interface RedditItemSelectionListener {
    void onItemSelected(String url);
}
