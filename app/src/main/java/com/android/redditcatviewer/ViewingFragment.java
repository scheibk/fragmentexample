package com.android.redditcatviewer;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.net.URL;

import butterknife.ButterKnife;
import butterknife.InjectView;



public class ViewingFragment extends Fragment {
    public static final String URL_PARAM = "url";

    @InjectView(R.id.webView)
    WebView webView;

    String url;
    boolean attached = false;

    public static ViewingFragment createInstance(String url) {
        ViewingFragment viewingFragment = new ViewingFragment();
        Bundle params = new Bundle();
        params.putString(ViewingFragment.URL_PARAM, url);
        viewingFragment.setArguments(params);

        return viewingFragment;
    }

    public ViewingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            url = getArguments().getString(URL_PARAM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_viewing, container, false);
        ButterKnife.inject(this, v);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        attached = true;

    }

    @Override
    public void onResume() {
        super.onResume();
        if(url != null) {
            webView.loadUrl(url);
        }
    }

    public void setUrl(String url) {
        if(attached) {
            webView.loadUrl(url);
        }
    }



}
