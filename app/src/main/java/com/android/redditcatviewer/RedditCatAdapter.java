package com.android.redditcatviewer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.android.reddit.Data_;
import com.android.reddit.RedditList;

import butterknife.ButterKnife;

/**
 * Created by kscheib on 4/15/15.
 */
public class RedditCatAdapter extends RecyclerView.Adapter<RedditCatAdapter.RedditCatViewHolder> {

    private RedditList redditList;
    private final RedditItemSelectionListener listener;

    public RedditCatAdapter(RedditList list, RedditItemSelectionListener listener) {
        this.redditList = list;
        this.listener = listener;
    }

    public static class RedditCatViewHolder extends RecyclerView.ViewHolder {

        public TextView titleView;
        public ImageView thumbView;
        public CatClickListener catClickListener;

        public RedditCatViewHolder(View view, RedditItemSelectionListener listener) {
            super(view);

            titleView = ButterKnife.findById(view, R.id.redditTitle);
            thumbView = ButterKnife.findById(view, R.id.thumbnail);
            catClickListener = new CatClickListener(listener);
            thumbView.setOnClickListener(catClickListener);
        }
    }

    @Override
    public RedditCatViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new RedditCatViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(RedditCatViewHolder holder, int position) {
        Data_ data = redditList.getData().getChildren().get(position).getData();

        holder.catClickListener.setUrl(data.getUrl());
        holder.titleView.setText(data.getTitle());
        ImageLoader.getInstance().displayImage(data.getThumbnail(), holder.thumbView);
    }

    @Override
    public int getItemCount() {
        return redditList.getData().getChildren().size();
    }

    private static class CatClickListener implements View.OnClickListener {
        private String url;
        private RedditItemSelectionListener listener;

        public CatClickListener(RedditItemSelectionListener listener) {
            this.listener = listener;
        }


        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public void onClick(View v) {
            if(url != null && listener != null) {
               listener.onItemSelected(url);
            }
        }
    }


}
