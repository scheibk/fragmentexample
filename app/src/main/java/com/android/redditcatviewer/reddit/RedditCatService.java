package com.android.redditcatviewer.reddit;

import com.android.reddit.RedditList;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by kscheib on 4/14/15.
 */
public interface RedditCatService {

    @GET("/r/cats.json")
    void getCatList(Callback<RedditList> callback);
}