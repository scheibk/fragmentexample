package com.android.redditcatviewer;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.reddit.RedditList;
import com.android.redditcatviewer.reddit.RedditCatService;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A placeholder fragment containing a simple view.
 */
public class RedditListFragment extends Fragment {

    @InjectView(R.id.my_recycler_view)
    RecyclerView recyclerView;
    private RedditCatAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    private RedditItemSelectionListener listener;

    public RedditListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_list, container, false);
        ButterKnife.inject(this, view);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        retrieveCatData();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            listener = (RedditItemSelectionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement RedditItemSelectionListener");
        }
    }

    private void retrieveCatData() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://www.reddit.com/")
                .build();

        RedditCatService catService = restAdapter.create(RedditCatService.class);

        catService.getCatList(new Callback<RedditList>() {
            @Override
            public void success(RedditList redditList, Response response) {
                Log.d(RedditListFragment.class.getName(), String.format("Retrieved %d cat posts.", redditList.getData().getChildren().size()));
                adapter = new RedditCatAdapter(redditList, listener);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(RedditListFragment.class.getName(), "Failed to get reddit list. ");
            }
        });

    }
}
