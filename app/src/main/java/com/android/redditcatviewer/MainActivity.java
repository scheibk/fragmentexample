package com.android.redditcatviewer;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class MainActivity extends AppCompatActivity implements RedditItemSelectionListener {

    private boolean dualPane = false;
    private boolean landscape = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dualPane = getResources().getBoolean(R.bool.dual_pane);
        landscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
        if(dualPane && landscape) {
            setContentView(R.layout.activity_main_dual_pane);
        } else {
            setContentView(R.layout.activity_main);
            RedditListFragment redditListFragment = new RedditListFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(redditListFragment.getClass().getName());
            transaction.replace(R.id.redditListContainer, redditListFragment);
            transaction.commit();
        }

        ImageLoaderConfiguration loaderConfiguration = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(new DisplayImageOptions.Builder()
                        .showImageForEmptyUri(android.R.color.transparent)
                        .showImageOnFail(android.R.color.transparent)
                        .showImageOnLoading(android.R.color.transparent)
                        .build()
                ).build();
        ImageLoader.getInstance().init(loaderConfiguration);
    }


    @Override
    public void onItemSelected(String url) {
        if(dualPane && landscape) {
            ViewingFragment webViewFragment = (ViewingFragment)getSupportFragmentManager().findFragmentById(R.id.webViewFragment);
            if(webViewFragment != null) {
                webViewFragment.setUrl(url);
            }
        } else {
            ViewingFragment webViewingFragment = ViewingFragment.createInstance(url);
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(webViewingFragment.getClass().getName());
            transaction.replace(R.id.redditListContainer, webViewingFragment);
            transaction.commit();
        }
    }

}
